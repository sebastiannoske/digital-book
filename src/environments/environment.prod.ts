export const environment = {
	production: true,
	cookieDomain: 'ifso.sowi.uni-due.de',
	chapterIndicatorAsProgress: true // true: progressbar, false: scrollbar
};
