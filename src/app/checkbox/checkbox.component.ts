import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit {
  @Input() index: number;
  @Input() modelItem: any;
  @Input() sectionName: string;
  @Input() cbName: string;
  @Input() disabled = false;
  @Output() notify: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  handleChange() {
    this.notify.emit(this.modelItem);
  }

}
