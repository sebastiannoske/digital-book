import { BSModalContext } from 'ad-ngx-modialog/plugins/bootstrap';

export class CustomModalContext extends BSModalContext {
    public chapters: any;
    public subChapters: any;
    public bookId: number;
    public chapterId: number;
    public sourceComponent: any;
    public link: string;
}
