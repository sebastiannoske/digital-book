import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss']
})
export class RadioComponent implements OnInit {
  @Input() sectionName: string;
  @Input() index: number;
  @Input() radioInfos: any;
  @Input() modelItem: any;
  @Output() notify: EventEmitter<any> = new EventEmitter<any>();


  constructor() { }

  ngOnInit() {
  }

  handleChange() {
    this.notify.emit(this.modelItem);
}

}
