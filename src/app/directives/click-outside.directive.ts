import { Directive, ElementRef, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
    selector: '[appClickOutside]'
})
export class ClickOutsideDirective {
    @Output() clickOutside: EventEmitter<any> = new EventEmitter<any>();

    constructor(private _elementRef: ElementRef) {
    }

    @HostListener('document:click', ['$event.target'])
    public onClick(targetElement: any) {
        if (this._elementRef) {
            const clickedInside = this._elementRef.nativeElement.contains(targetElement);

            if (!clickedInside) {
                this.clickOutside.emit(targetElement);
            }
        }
    }
}
