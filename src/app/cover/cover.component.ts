import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-cover',
	templateUrl: './cover.component.html',
	styleUrls: ['./cover.component.scss']
})
export class CoverComponent implements OnInit {
	@Input() cover: any;
	@Output() coverImageLoaded: EventEmitter<any> = new EventEmitter<any>();
	body: any;

	constructor() {
		this.body = document.body;
	}

	ngOnInit() {}

	imageLoaded(event: any, imageWrap: any) {
		if (event.reason === 'finally') {
			imageWrap.classList.add('image-loaded');
			this.coverImageLoaded.emit({ loaded: true });
		}
	}

	public onInViewportChange(inViewport: boolean) {
		if (inViewport) {
			if (this.body.classList.contains('text-section')) {
				this.body.classList.remove('text-section');
			}
		} else if (!this.body.classList.contains('text-section')) {
			this.body.classList.add('text-section');
		}
	}
}
