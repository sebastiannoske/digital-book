import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { DataService } from './data.service';
import {
	NgcCookieConsentService,
	NgcInitializeEvent,
	NgcStatusChangeEvent,
	NgcNoCookieLawEvent
} from 'ngx-cookieconsent';
import { CookieService } from 'ngx-cookie-service';
import { HelperService } from './services/helper.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-root',
	changeDetection: ChangeDetectionStrategy.OnPush,
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	providers: [NgcCookieConsentService]
})
export class AppComponent implements OnInit {
	private popupOpenSubscription: Subscription;
	private popupCloseSubscription: Subscription;
	private initializeSubscription: Subscription;
	private statusChangeSubscription: Subscription;
	private revokeChoiceSubscription: Subscription;
	private noCookieLawSubscription: Subscription;
	bannerHasBeenDismissed = false;

	constructor(
		private _dataService: DataService,
		private ccService: NgcCookieConsentService,
		private cookieService: CookieService,
		private helperService: HelperService
	) {}

	ngOnInit() {
		this._dataService.getData(1);
		this.helperService.setOptionalCookiesAllowed(
			this.cookieService.get('cookieconsent_status') === 'allow'
		);
		this.initCookieSubscriptions();
	}

	private initCookieSubscriptions() {
		// subscribe to cookieconsent observables to react to main events
		this.popupOpenSubscription = this.ccService.popupOpen$.subscribe(() => {
			// you can use this.ccService.getConfig() to do stuff...
		});

		this.popupCloseSubscription = this.ccService.popupClose$.subscribe(() => {
			// you can use this.ccService.getConfig() to do stuff...
		});

		this.initializeSubscription = this.ccService.initialize$.subscribe(
			(event: NgcInitializeEvent) => {
				// you can use this.ccService.getConfig() to do stuff...
			}
		);

		this.statusChangeSubscription = this.ccService.statusChange$.subscribe(
			(event: NgcStatusChangeEvent) => {
				// you can use this.ccService.getConfig() to do stuff...
				const allowCookies = event.status === 'allow';
				this.helperService.setOptionalCookiesAllowed(allowCookies);

				if (!allowCookies) {
					this.removeExistingCookies();
				}
			}
		);

		this.revokeChoiceSubscription = this.ccService.revokeChoice$.subscribe(() => {
			// you can use this.ccService.getConfig() to do stuff...
		});

		this.noCookieLawSubscription = this.ccService.noCookieLaw$.subscribe(
			(event: NgcNoCookieLawEvent) => {
				// you can use this.ccService.getConfig() to do stuff...
			}
		);
	}

	ngOnDestroy() {
		this.popupOpenSubscription.unsubscribe();
		this.popupCloseSubscription.unsubscribe();
		this.initializeSubscription.unsubscribe();
		this.statusChangeSubscription.unsubscribe();
		this.revokeChoiceSubscription.unsubscribe();
		this.noCookieLawSubscription.unsubscribe();
	}

	private removeExistingCookies() {
		this.cookieService.deleteAll();
	}
}
