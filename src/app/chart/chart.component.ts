import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Chart } from 'chart.js';
import 'chartjs-plugin-deferred';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  @Input() item: any;
  @ViewChild('myCanvas', { static: true }) chartWrap: ElementRef;
  ctx: CanvasRenderingContext2D;
  chartData: any;

  constructor() {
    Chart.defaults.global.animation.duration = 5000;
    this.chartData = {};
  }

  ngOnInit() {
    this.chartData.type = this.item.chart.type;

    this.chartData.data = {};
    this.chartData.data.labels = this.item.chart.chart_labels.map(item => {
      return item.label;
    });

    this.chartData.data.datasets = [];

    this.item.chart.chart_datasets.forEach(dataSet => {
      this.chartData.data.datasets.push(
        {
          label: dataSet.label,
          data: dataSet.chartItems.map(item => {
            return item.value;
          }),
          backgroundColor: (this.chartData.type === 'line')
            ? dataSet.chartItems[0].color
            : dataSet.chartItems.map(item => {
              return item.color;
            }),
          borderColor: (this.chartData.type === 'line')
            ? dataSet.chartItems[0].color
            : (this.chartData.type === 'pie')
              ? 'white'
              : dataSet.chartItems.map(item => {
                return item.color;
              }),
          'borderWidth': (this.chartData.type === 'line') ? 2 : 1,
          'fill': false
        }
      );
    });

    if (this.chartData.type === 'bar' || this.chartData.type === 'line') {
      this.chartData.options = {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              min: this.item.chart.y_axis_min
            }
          }]
        }
      };

      if (this.item.chart.y_axis_max > 0) {
        this.chartData.options.scales.yAxes[0].ticks.max = this.item.chart.y_axis_max;
      }

      if (this.item.chart.label_x_axis && this.item.chart.label_x_axis.length) { // there is a x axis label given
        this.chartData.options.scales.xAxes = [{
          scaleLabel: {
            display: true,
            labelString: this.item.chart.label_x_axis
          }
        }];
      }

      if (this.item.chart.label_y_axis && this.item.chart.label_y_axis.length) { // there is a y axis label given
        this.chartData.options.scales.yAxes[0].scaleLabel = {
          display: true,
          labelString: this.item.chart.label_y_axis
        };
      }

    }

    if (this.chartWrap) {
      this.ctx = (<HTMLCanvasElement>this.chartWrap.nativeElement).getContext('2d');
      const myChart = new Chart(
        this.ctx,
        this.chartData
      );
    }
  }
}
