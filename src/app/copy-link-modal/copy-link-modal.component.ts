import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DialogRef, ModalComponent } from 'ad-ngx-modialog';
import { CustomModalContext } from '../custom-modal-context';

@Component({
  selector: 'app-copy-link-modal',
  templateUrl: './copy-link-modal.component.html',
  styleUrls: ['./copy-link-modal.component.scss']
})
export class CopyLinkModalComponent implements OnInit, ModalComponent<CustomModalContext> {
  @ViewChild('linkToCopy', { static: true }) linkToCopy: ElementRef;
  context: CustomModalContext;
  modalVisible: boolean;
  link: string;

  constructor(public dialog: DialogRef<CustomModalContext>) { }

  ngOnInit() {
    this.link = this.dialog.context.link;

    setTimeout(() => {
      this.modalVisible = true;
    }, 500);
  }

  clickedOutside(event: any) {
    if (this.modalVisible) {
      this.dialog.close();
    }
  }

  copyLink() {

    window.getSelection().removeAllRanges();
    const range = document.createRange();
    range.selectNode(this.linkToCopy.nativeElement);
    window.getSelection().addRange(range);

    try {
      document.execCommand('copy');
      setTimeout(() => {
        this.dialog.close();
      }, 300);
    } catch (err) {
      console.log('Oops, unable to copy');
    }

    window.getSelection().removeAllRanges();
  }

}
