import { Component, OnInit, Input } from '@angular/core';
import { EmbedVideoService } from 'ngx-embed-video';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {
  @Input() item: any;
  iframeHtml: any;
  iframeVisible = false;

  constructor(private _embedVideoService: EmbedVideoService) { }

  ngOnInit() {
    this.iframeHtml = this._embedVideoService.embed(this.item.source);
  }

  onInViewportChange(inViewport: true, videoWrap: any) {
    if (inViewport && !this.iframeVisible) {
      this.iframeVisible = true;
      videoWrap.classList.add('in-viewport');
    }
  }
}
