import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataService } from '../data.service';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {
  body: any;
  book: any;
  bookCover: any;
  coverImageLoaded: boolean;
  isSearchMode: boolean;
  dashBoardOpen: boolean;

  constructor(
    private _dataService: DataService,
    private helperService: HelperService,
    private _changeDetector: ChangeDetectorRef
  ) {
    this.body = document.body;
    this.coverImageLoaded = false;
    this.isSearchMode = false;
  }

  ngOnInit() {
    this._dataService.bookData.subscribe(bookData => {
      if (bookData.status && bookData.status === 'success') {
        this.book = bookData.book;

        this.setLearningSectionPre();

        this.bookCover = {
          title: this.book.title,
          author: this.book.author,
          color: this.book.color,
          image: this.book.image
        };

        if (this.book.chapters.length > 0) {
          this.body.classList.add('chapters-' + this.book.chapters.length);
        }
        this._changeDetector.markForCheck();
      }
    });

    this.helperService.filterData.subscribe(filterData => {
      if (filterData.value !== undefined) {
        this.isSearchMode = filterData.value.length ? true : false;
      }
    });

    this.helperService.dashbordStateData.subscribe(dashbordStateData => {
      this.dashBoardOpen = dashbordStateData.open;
      this._changeDetector.markForCheck();
    });

    window.scrollTo(0, 0);
  }

  private setLearningSectionPre() {
    let chapterIndex = 1;

    this.book.chapters.forEach(chapter => {
      let subChapterIndex = 1;

      chapter.sub_chapters.forEach(subChapter => {
        let learningSectionIndex = 1;

        subChapter.learningSections.forEach(learningSection => {
          let workOrderIndex = 1;

          if (learningSection.is_learning_section) {

            if (learningSection.workOrders.length) {

              learningSection.workOrders.forEach(workOrder => {

                workOrder.workOrderIndex = chapterIndex + '.' + subChapterIndex + '.' + learningSectionIndex + '.' + workOrderIndex;
                workOrderIndex++;

              });
            }
          }

          learningSection.learningSectionIndex = chapterIndex + '.' + subChapterIndex + '.' + learningSectionIndex;
          learningSection.learningSectionId = 'lernabschnitt-' + chapterIndex + '-' + subChapterIndex + '-' + learningSectionIndex;
          learningSectionIndex++;

        });

        subChapterIndex++;
      });

      chapterIndex++;
    });

  }

  coverImageLoadedEvent(event: any) {
    if (event && event.loaded) {
      this.coverImageLoaded = true;
    }
  }

  openDashboard() {
    this.helperService.setDashbordState(true);
  }

}
