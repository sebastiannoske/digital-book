import { BookRoutingModule } from './book-routing.module';

describe('BookRoutingModule', () => {
  let bookRoutingModule: BookRoutingModule;

  beforeEach(() => {
    bookRoutingModule = new BookRoutingModule();
  });

  it('should create an instance', () => {
    expect(bookRoutingModule).toBeTruthy();
  });
});
