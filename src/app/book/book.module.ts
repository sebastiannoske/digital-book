import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookRoutingModule } from './book-routing.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { EmbedVideo } from 'ngx-embed-video';
import { InViewportModule } from '@thisissoon/angular-inviewport';
import { ModalModule } from 'ad-ngx-modialog';
import { BootstrapModalModule } from 'ad-ngx-modialog/plugins/bootstrap';
import { FormsModule } from '@angular/forms';
import { KatexModule } from 'ng-katex';

import { BookComponent } from './book.component';
import { ChapterComponent } from '../chapter/chapter.component';
import { SubChapterComponent } from '../sub-chapter/sub-chapter.component';
import { ImageComponent } from '../image/image.component';
import { TextComponent } from '../text/text.component';
import { VideoComponent } from '../video/video.component';
import { IframeComponent } from '../iframe/iframe.component';
import { QuoteComponent } from '../quote/quote.component';
import { ChartComponent } from '../chart/chart.component';
import { InfoBoxComponent } from '../info-box/info-box.component';
import { CoverComponent } from '../cover/cover.component';
import { DashbardComponent } from '../dashbard/dashbard.component';
import { LearningSectionComponent } from '../learning-section/learning-section.component';
import { ChapterScrollBarComponent } from '../chapter-scroll-bar/chapter-scroll-bar.component';
import { ChapterOverviewComponent } from '../chapter-overview/chapter-overview.component';
import { DownloadModalComponent } from '../download-modal/download-modal.component';
import { CopyLinkModalComponent } from '../copy-link-modal/copy-link-modal.component';
import { CheckboxComponent } from '../checkbox/checkbox.component';
import { RadioComponent } from '../radio/radio.component';
import { SearchBarComponent } from '../search-bar/search-bar.component';

import { ClickOutsideDirective } from '../directives/click-outside.directive';
import { HighlightPipe } from '../pipes/search-highlight.pipe';

@NgModule({
	imports: [
		CommonModule,
		BookRoutingModule,
		LazyLoadImageModule,
		EmbedVideo.forRoot(),
		InViewportModule,
		ModalModule.forRoot(),
		BootstrapModalModule,
		FormsModule,
		KatexModule
	],
	declarations: [
		BookComponent,
		ChapterComponent,
		SubChapterComponent,
		ImageComponent,
		TextComponent,
		VideoComponent,
		IframeComponent,
		QuoteComponent,
		ChartComponent,
		InfoBoxComponent,
		CoverComponent,
		DashbardComponent,
		LearningSectionComponent,
		ChapterScrollBarComponent,
		ChapterOverviewComponent,
		DownloadModalComponent,
		CopyLinkModalComponent,
		CheckboxComponent,
		RadioComponent,
		SearchBarComponent,
		ClickOutsideDirective,
		HighlightPipe
	],
	entryComponents: [DownloadModalComponent, CopyLinkModalComponent]
})
export class BookModule {}
