import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-image',
	templateUrl: './image.component.html',
	styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {
	@Input() item: any;

	constructor() {}

	ngOnInit() {}

	imageLoaded(event: any, imageWrap: any) {
		if (event.reason === 'finally') {
			imageWrap.classList.add('image-loaded');
		}
	}
}
