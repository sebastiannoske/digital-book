import { Component, OnInit, Input } from '@angular/core';
import { ScrollToService } from '../services/scroll-to.service';

@Component({
  selector: 'app-chapter-overview',
  templateUrl: './chapter-overview.component.html',
  styleUrls: ['./chapter-overview.component.scss']
})
export class ChapterOverviewComponent implements OnInit {
  @Input() chapters: any[];
  @Input() chapterIndex: number;

  constructor(private _scrollToService: ScrollToService) { }

  ngOnInit() {}

  public scrollToChapter(chapterIndex: number) {
    const chapterWrap = document.getElementById('chapter-' + chapterIndex);
    if (chapterWrap) {
      this._scrollToService.scrollTo(chapterWrap.offsetTop, 0);
    }
  }

}
