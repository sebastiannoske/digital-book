import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageContentResolver } from './page-content/page-content.resolver';

const routes: Routes = [
	{
		path: 'impressum',
		loadChildren: () => import('./page-content/page-content.module').then(m => m.PageContentModule),
		resolve: { pageContentData: PageContentResolver }
	},
	{
		path: 'datenschutz',
		loadChildren: () => import('./page-content/page-content.module').then(m => m.PageContentModule),
		resolve: { pageContentData: PageContentResolver }
	},
	{
		path: 'copyright',
		loadChildren: () => import('./page-content/page-content.module').then(m => m.PageContentModule),
		resolve: { pageContentData: PageContentResolver }
	},
	{ path: '', loadChildren: () => import('./book/book.module').then(m => m.BookModule) },
	{ path: '**', redirectTo: '' }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
	providers: [PageContentResolver]
})
export class AppRoutingModule {}
