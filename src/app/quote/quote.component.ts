import { Component, OnInit, Input } from '@angular/core';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.scss']
})
export class QuoteComponent implements OnInit {
  @Input() item: any;
  filter: string;

  constructor(private _helperService: HelperService) { }

  ngOnInit() {
    this._helperService.filterData.subscribe(filterData => {
      this.filter = filterData.value;
    });
  }

}
