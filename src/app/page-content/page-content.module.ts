import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageContentRoutingModule } from './page-content-routing.module';

import { PageContentComponent } from './page-content.component';

@NgModule({
  imports: [
    CommonModule,
    PageContentRoutingModule
  ],
  declarations: [
    PageContentComponent
  ],
  entryComponents: [
  ]
})
export class PageContentModule { }
