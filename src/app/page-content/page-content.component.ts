import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-page-content',
  templateUrl: './page-content.component.html',
  styleUrls: ['./page-content.component.scss']
})
export class PageContentComponent implements OnInit {
  text: any;

  constructor(private _route: ActivatedRoute) { }

  ngOnInit() {
    this.text = this._route.snapshot.data['pageContentData'].content.text;
    window.scrollTo(0, 0);
  }

}
