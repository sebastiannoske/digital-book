import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
    Resolve,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Router
} from '@angular/router';
import { DataService } from '../data.service';

@Injectable()
export class PageContentResolver implements Resolve<any> {
    constructor(
        private _router: Router,
        private _dataService: DataService
    ) {}

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> {
        const id = route.params['id'];
        const path = route.routeConfig.path;

        return this._dataService.fetchPageContentData(path);
    }
}
