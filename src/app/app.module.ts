import { environment } from './../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NgcCookieConsentModule, NgcCookieConsentConfig } from 'ngx-cookieconsent';
import { CookieService } from 'ngx-cookie-service';

const cookieConfig: NgcCookieConsentConfig = {
	cookie: {
		domain: environment.cookieDomain
	},
	position: 'top',
	theme: 'edgeless',
	palette: {
		popup: {
			background: '#4a90e2',
			text: '#ffffff',
			link: '#ffffff'
		},
		button: {
			background: '#f1d600',
			text: '#000000',
			border: 'transparent'
		}
	},
	type: 'opt-in',
	content: {
		message: `<span>Unsere Webseite bindet Inhalte von externen Webseiten ein. Das machen wir, um zum Beispiel Videos oder Lernanwendungen anbieten zu können. Diese externen Webseiten setzen Cookies ein. Wenn du Inhalte von externen Webseiten angezeigt bekommen möchtest, musst du auf „Alle Cookies zulassen“ klicken. In unserer <a href="/datenschutz" target="_blank">Datenschutzerklärung</a> haben wir Informationen dazu gesammelt, welche externen Inhalte wir einbinden. Bitte lies dir diese Informationen durch.</span>
		<span>Wenn du auf „Nur notwendige Cookies zulassen“ klickst, werden nur Cookies gesetzt, die für das Funktionieren unserer Webseite unverzichtbar sind. Inhalte von externen Webseiten kannst Du dann nicht sehen. Du kannst deine Entscheidung jederzeit ändern. Möglicherweise musst du unsere Seite erneut laden, damit deine Auswahl angenommen wird.</span>
		<span>Du kannst auch in den Einstellungen deines Browsers festlegen, wie er mit Cookies umgehen soll. Dort kannst du auch gespeicherte Cookies wieder löschen. Mehr erfährst Du in der „Hilfe“ deines Browsers.</span>
		`,
		dismiss: 'Verstanden!',
		deny: 'Nur notwendige Cookies zulassen',
		allow: 'Alle Cookies zulassen',
		link: 'Erfahre mehr...',
		href: 'https://cookiesandyou.com',
		policy: 'Cookie Einstellungen'
	}
};

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserAnimationsModule,
		HttpClientModule,
		AppRoutingModule,
		NgcCookieConsentModule.forRoot(cookieConfig)
	],
	providers: [CookieService],
	bootstrap: [AppComponent]
})
export class AppModule {}
