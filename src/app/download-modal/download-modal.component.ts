import { Component, OnInit } from '@angular/core';
import { DialogRef, ModalComponent } from 'ad-ngx-modialog';
import { CustomModalContext } from '../custom-modal-context';
import { DataService } from '../data.service';

@Component({
  selector: 'app-download-modal',
  templateUrl: './download-modal.component.html',
  styleUrls: ['./download-modal.component.scss']
})
export class DownloadModalComponent implements OnInit, ModalComponent<CustomModalContext> {
  context: CustomModalContext;
  chapters: any[];
  subChapters: any[];
  radios: any[];
  modalVisible: boolean;
  allChaptersString: string;
  chosenChaptersString: string;
  allSubChaptersString: string;
  chosenSubChaptersString: string;
  chapterId: number;
  printFormats: any[];
  radioModel: any = { checkedValue: 'generate-pdf' };
  sourceComponent: any;

  constructor(public dialog: DialogRef<CustomModalContext>, private _dataService: DataService) {
    this.radios = [];
  }

  ngOnInit() {
    this.chapters = this.dialog.context.chapters;
    this.subChapters = this.dialog.context.subChapters;
    this.sourceComponent = this.dialog.context.sourceComponent;

    if (this.chapters) {
      this.generateChapterRadios();
    } else if (this.subChapters) {
      this.chapterId = this.dialog.context.chapterId;
      this.generateSubChapterRadios();
    }

    this.printFormats = [
      {
        name: 'PDF',
        value: 'generate-pdf'
      },
      {
        name: 'ODT',
        value: 'generate-odt'
      }
    ];

    setTimeout(() => {
      this.modalVisible = true;
    }, 500);
  }

  generateChapterRadios() {
    this.allChaptersString = '';
    let appended = false;

    this.chapters.forEach((chapter) => {
      this.radios.push({
        id: chapter.id,
        name: chapter.title,
        checked: true
      });

      this.allChaptersString += !appended ? chapter.id : ',' + chapter.id;

      appended = true;
    });

    this.chosenChaptersString = this.allChaptersString;
  }

  generateSubChapterRadios() {
    this.allChaptersString = '';
    this.allSubChaptersString = '';
    let appended = false;

    this.subChapters.forEach((subChapter) => {
      subChapter.learningSections.forEach((learningSection) => {

        if (learningSection.is_learning_section) {
          this.radios.push({
            id: learningSection.id,
            name: learningSection.title,
            checked: true
          });

          this.allSubChaptersString += !appended ? learningSection.id : ',' + learningSection.id;

          appended = true;
        }


      });
    });

    this.allChaptersString += this.chapterId;
    this.chosenChaptersString = this.allChaptersString;
    this.chosenSubChaptersString = this.allSubChaptersString;
  }

  handleChange(event: any) {
    if (this.chapters) {
      this.chosenChaptersString = '';
      let appended = false;

      this.radios.forEach((radio) => {
        if (radio.checked) {
          this.chosenChaptersString += !appended ? radio.id : ',' + radio.id;
          appended = true;
        }
      });
    } else if (this.subChapters) {
      this.chosenSubChaptersString = '';
      let appended = false;

      this.radios.forEach((radio) => {
        if (radio.checked) {
          this.chosenSubChaptersString += !appended ? radio.id : ',' + radio.id;
          appended = true;
        }
      });
    }
  }

  handleChangeFormat() {
    // console.log(this.radioModel);
  }

  clickedOutside(event: any) {
    if (this.modalVisible) {
      this.sourceComponent.modalClosedHandler();
      this.dialog.close();
    }
  }

  generateDownload() {
    this._dataService.generateDownload(this.dialog.context.bookId).subscribe(response => {
      this.downloadFile(response);
    });
  }

  downloadFile(data: any) {
    const blob = new Blob([data], { type: 'text/csv' });
    const url = window.URL.createObjectURL(blob);
    window.open(url);
  }
}
