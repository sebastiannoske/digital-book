import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ScrollToService } from '../services/scroll-to.service';
import { HelperService } from '../services/helper.service';

@Component({
	selector: 'app-text',
	templateUrl: './text.component.html',
	styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit {
	@ViewChild('text', { static: true }) text: ElementRef;
	@Input() item: any;
	filter: string;
	textStyleClass = 'content-text ';

	constructor(private _scrollToService: ScrollToService, private _helperService: HelperService) {}

	ngOnInit() {
		this.checkTextStyle();
		setTimeout(() => {
			if (this.text) {
				const scrollToTextes = this.findAncestor(this.text.nativeElement, '.scroll-to');
				if (scrollToTextes) {
					scrollToTextes.onclick = e => {
						e.preventDefault();
						if (e.target.hash && e.target.hash.length) {
							const pos = this.getGlobalOffset(document.getElementById(e.target.hash.substr(1))).top;
							if (pos) {
								this._scrollToService.scrollTo(pos, 0);
							}
						}
					};
				}
			}
		}, 0);

		this._helperService.filterData.subscribe(filterData => {
			this.filter = filterData.value;
		});
	}

	private checkTextStyle() {
		let helperIndex = this.item.text.indexOf('style=');
		if (helperIndex >= 0) {
			helperIndex += 6;
			const helperIndex2 = this.item.text.indexOf(';', helperIndex);
			const rule = this.item.text.substr(helperIndex, helperIndex2 - helperIndex);
			const parts = rule.split(':');
			if (parts && parts.length === 2) {
				this.textStyleClass += parts[1].trim();
			}
		}
	}

	private findAncestor(el, cls) {
		return el.querySelector(cls);
	}

	private getGlobalOffset(el) {
		let x = 0,
			y = 0;

		while (el) {
			x += el.offsetLeft;
			y += el.offsetTop;
			el = el.offsetParent;
		}
		return { left: x, top: y };
	}
}
