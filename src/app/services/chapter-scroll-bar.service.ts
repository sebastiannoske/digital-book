import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

const colors: any = ['#D0021B', '#F5A623', '#F8E71C', '#B8E986', '#50E3C2', '#4A90E2'];

@Injectable({
  providedIn: 'root'
})
export class ChapterScrollBarService {
  private _state: BehaviorSubject<any> = new BehaviorSubject(<any>{});

  constructor() { }

  setCurrentState(chapterIndex: number, chapterHeight: number, offsetTop: number, fixed: boolean) {
    this._state.next(
      {
        chapterIndex: chapterIndex,
        chapterHeight: chapterHeight,
        offsetTop: offsetTop,
        fixed: fixed,
        color: colors[chapterIndex - 1]
      }
    );
  }

  public get currentState(): BehaviorSubject<any> {
    return this._state;
  }
}
