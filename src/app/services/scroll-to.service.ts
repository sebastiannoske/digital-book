import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScrollToService {
  constructor() {}

  public scrollTo(offsetTop: number, scrollDuration: number) {
    window.scrollTo(0, offsetTop);
  }
}
