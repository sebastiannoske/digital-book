import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class HelperService {
	private filter: BehaviorSubject<any> = new BehaviorSubject({});
	private domResults: BehaviorSubject<any> = new BehaviorSubject({});
	private index: BehaviorSubject<any> = new BehaviorSubject({});
	private dashbordState: BehaviorSubject<any> = new BehaviorSubject({});
	private lastScrollTop: BehaviorSubject<any> = new BehaviorSubject({});
	private activeSection: BehaviorSubject<any> = new BehaviorSubject({});
	private optionalCookiesAllowedObjext: BehaviorSubject<boolean> = new BehaviorSubject(false);
	private lastScrollTopObject: any;

	constructor() {
		this.lastScrollTopObject = { lastValue: 0, direction: 1 };
		this.lastScrollTop.next(this.lastScrollTopObject);
		window.addEventListener('scroll', this.onWindowScroll.bind(this), false);
	}

	public setSearchObject(object: any) {
		this.filter.next({ value: object.filter, isClosed: object.isClosed });
	}

	public setSearchDomResults(domResults: any) {
		this.domResults.next({ domResults: domResults });
	}

	public setOptionalCookiesAllowed(value: boolean) {
		this.optionalCookiesAllowedObjext.next(value);
	}

	public setSearchDomResultsIndex(index: number) {
		this.index.next({ value: index });
	}

	public setDashbordState(open: boolean) {
		this.dashbordState.next({ open: open });
	}

	public setActiveSection(activeSection: any) {
		this.activeSection.next(activeSection);
	}

	public get filterData(): Observable<any> {
		return this.filter;
	}

	public get domResultsData(): Observable<any> {
		return this.domResults;
	}

	public get optionalCookiesAllowed(): Observable<any> {
		return this.optionalCookiesAllowedObjext;
	}

	public get indexData(): Observable<any> {
		return this.index;
	}

	public get dashbordStateData(): Observable<any> {
		return this.dashbordState;
	}

	public get lastScrollTopData(): Observable<any> {
		return this.lastScrollTop;
	}

	public get activeSectionData(): Observable<any> {
		return this.activeSection;
	}

	onWindowScroll() {
		const scrollTop = document.scrollingElement.scrollTop || document.documentElement.scrollTop;

		if (this.lastScrollTopObject.direction === 0 && scrollTop > this.lastScrollTopObject.lastValue) {
			// scroll direction changed to down
			this.lastScrollTopObject.direction = 1;
			this.lastScrollTop.next(this.lastScrollTopObject);
		} else if (
			this.lastScrollTopObject.direction === 1 &&
			scrollTop < this.lastScrollTopObject.lastValue
		) {
			// scroll direction changed to top
			this.lastScrollTopObject.direction = 0;
			this.lastScrollTop.next(this.lastScrollTopObject);
		}

		this.lastScrollTopObject.lastValue = scrollTop;
	}
}
