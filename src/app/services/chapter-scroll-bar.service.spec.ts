import { TestBed, inject } from '@angular/core/testing';

import { ChapterScrollBarService } from './chapter-scroll-bar.service';

describe('ChapterScrollBarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChapterScrollBarService]
    });
  });

  it('should be created', inject([ChapterScrollBarService], (service: ChapterScrollBarService) => {
    expect(service).toBeTruthy();
  }));
});
