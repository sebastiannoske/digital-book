import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ChapterScrollBarService } from '../services/chapter-scroll-bar.service';
import { HelperService } from '../services/helper.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-chapter',
  templateUrl: './chapter.component.html',
  styleUrls: ['./chapter.component.scss']
})
export class ChapterComponent implements OnInit {
  @ViewChild('chapterWrap', { static: true }) chapterWrap: ElementRef;
  @Input() chapters: any[];
  @Input() chapterIndex: any;
  chapter: any;
  inViewPort = false;
  previousIndex: number;
  scrollBarHelperHeight: any;
  body: any;
  introWrapHasBeenVisited: boolean;
  lastTopOffset: any;
  nextIsText: boolean;

  constructor(private _chapterScrollBarService: ChapterScrollBarService, private helperService: HelperService) {
    this.previousIndex = 0;
    this.scrollBarHelperHeight = 0;
    this.body = document.body;
    this.introWrapHasBeenVisited = false;
    this.nextIsText = false;
  }

  ngOnInit() {
    this.chapter = this.chapters[this.chapterIndex - 1];

    this._chapterScrollBarService.currentState.subscribe(chapterState => {
      this.previousIndex = chapterState.chapterIndex - 1;
    });

    this.helperService.lastScrollTopData.subscribe(lastScrollTopData => {
      if (lastScrollTopData && lastScrollTopData.direction !== undefined) {
        this.lastTopOffset = lastScrollTopData;
      }
    });
  }

  public setBodyClass(inViewport: boolean, isText: boolean) {
    if (this.lastTopOffset.direction === 0) { // scroll top
      if (inViewport && !isText && this.body.classList.contains('text-section')) {
        this.body.classList.remove('text-section');
      } else if (inViewport && isText && !this.body.classList.contains('text-section')) {
        this.body.classList.add('text-section');
      }
    } else if (this.lastTopOffset.direction === 1) { // scroll down
      if (inViewport && !isText) {
        setTimeout(() => {
          this.nextIsText = isText;
        }, 0);
      }
      if (!inViewport && !isText && !this.body.classList.contains('text-section')) {
          this.body.classList.add('text-section');
      } else if (!inViewport && isText && !this.nextIsText && this.body.classList.contains('text-section')) {
        this.body.classList.remove('text-section');
        this.nextIsText = true;
      }
    }
  }

  public onInViewportChange(inViewport: boolean, chapterHeight: number) {
    if (inViewport) {
      this.inViewPort = true;
      this.scrollBarHelperHeight = (environment.chapterIndicatorAsProgress)
          ? document.documentElement.clientHeight - 30
          : (document.documentElement.clientHeight / chapterHeight) * (document.documentElement.clientHeight - 30);
      const rect = this.chapterWrap.nativeElement.getBoundingClientRect();
      this._chapterScrollBarService.setCurrentState(
        this.chapterIndex,
        chapterHeight,
        rect.top +
        (window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop) -
        document.documentElement.clientTop,
        this.inViewPort
      );
    } else if (this.inViewPort) {
      this.inViewPort = false;
      const rect = this.chapterWrap.nativeElement.getBoundingClientRect();
      this._chapterScrollBarService.setCurrentState(
        this.chapterIndex,
        chapterHeight,
        rect.top +
        (window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop) -
        document.documentElement.clientTop,
        this.inViewPort
      );
    }

  }

  public setActiveChapter(inViewport: boolean) {
    if (inViewport) {
      this.helperService.setActiveSection({
        chapterIndex: this.chapterIndex - 1,
        subChapterId: null,
        learningSectionId: null
      });
    }
  }

}
