import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { overlayConfigFactory } from 'ad-ngx-modialog';
import { Modal, BSModalContext } from 'ad-ngx-modialog/plugins/bootstrap';
import { CopyLinkModalComponent } from '../copy-link-modal/copy-link-modal.component';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-learning-section',
  templateUrl: './learning-section.component.html',
  styleUrls: ['./learning-section.component.scss'],
  animations: [
    trigger('showAdditional', [
      transition('void => *', [
        animate('.5s ease',
          keyframes([
            style({ height: '0px', opacity: '0', transform: 'translateY(25px)', offset: 0 }),
            style({ height: '*', opacity: '0', transform: 'translateY(25px)', offset: .7 }),
            style({ height: '*', opacity: '1', transform: 'translateX(0)', offset: 1 })
          ])
        )
      ]),
      transition('* => void', [
        animate('.5s ease',
          keyframes([
            style({ height: '*', opacity: '1', transform: 'translateY(0)', offset: 0 }),
            style({ height: '*', opacity: '0', transform: 'translateY(15px)', offset: .3 }),
            style({ height: '0px', opacity: '0', transform: 'translateY(15px)', offset: 1 })
          ])
        )
      ])
    ])
  ]
})
export class LearningSectionComponent implements OnInit {
  @Input() learningSection: any;
  @Input() chapterIndex: number;
  @Input() chapterId: number;
  @Input() subChapterId: number;
  activeAdditionalIndex: number;
  filter: string;

  constructor(public _modal: Modal, private _helperService: HelperService) {
    this.activeAdditionalIndex = 0;
  }

  ngOnInit() {
    this._helperService.filterData.subscribe(filterData => {
      this.filter = filterData.value;
    });
  }

  setActiveAdditionalIndex(index: number) {
    if (this.activeAdditionalIndex !== index) {
      this.activeAdditionalIndex = index;
    } else {
      this.activeAdditionalIndex = 0;
    }

  }

  openCopyLinkModal(link: string) {
    const finalLink = window.location.origin + window.location.pathname + '#' + link;

    return this._modal.open(CopyLinkModalComponent, overlayConfigFactory(
      { link: finalLink }, BSModalContext));
  }

  setActiveSection(inViewport: boolean) {
    if (inViewport) {
      if (this.learningSection.is_learning_section) {
        this._helperService.setActiveSection({
          chapterIndex: this.chapterIndex - 1,
          subChapterId: this.subChapterId,
          learningSectionId: this.learningSection.id
        });
      }
    }
  }
}
