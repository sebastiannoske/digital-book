import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { HelperService } from '../services/helper.service';

@Component({
	selector: 'app-iframe',
	templateUrl: './iframe.component.html',
	styleUrls: ['./iframe.component.scss']
})
export class IframeComponent implements OnInit {
	@Input() item: any;
	iframeSource: SafeUrl;
	iframeVisible = false;
	paddingTop = 0;
	optionalCookiesAllowed = false;

	constructor(private _sanitizer: DomSanitizer, private helperService: HelperService) {}

	ngOnInit() {
		if (this.item.source && this.item.source.length) {
			if (this.item.source.indexOf('width/') > 0 && this.item.source.indexOf('height/') > 0) {
				// Geoalgebra iframe
				const widthStartIndex = this.item.source.indexOf('width/') + 6;
				const widthEndIndex = this.item.source.indexOf('/', widthStartIndex);
				const heightStartIndex = this.item.source.indexOf('height/') + 7;
				const heightEndIndex = this.item.source.indexOf('/', heightStartIndex);
				const givenWidth = parseInt(this.item.source.substring(widthStartIndex, widthEndIndex), 10);
				const givenHeight = parseInt(this.item.source.substring(heightStartIndex, heightEndIndex), 10);

				if (givenWidth && givenWidth > 0 && givenHeight && givenHeight > 0) {
					this.paddingTop = 100 * (givenHeight / givenWidth);
				}
			}

			this.iframeSource = this.iframeURL(this.item.source);
		}

		this.helperService.optionalCookiesAllowed.subscribe(optionalCookiesAllowed => {
			this.optionalCookiesAllowed = optionalCookiesAllowed;
		});
	}

	iframeURL(iframeUrl: string) {
		return this._sanitizer.bypassSecurityTrustResourceUrl(iframeUrl);
	}

	onInViewportChange(inViewport: boolean, videoWrap: any) {
		if (inViewport && !this.iframeVisible) {
			this.iframeVisible = true;
			videoWrap.classList.add('in-viewport');
		}
	}
}
