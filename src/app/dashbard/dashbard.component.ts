import { Component, OnInit, Input } from "@angular/core";
import {
  trigger,
  style,
  transition,
  animate,
  keyframes
} from "@angular/animations";
import { ScrollToService } from "../services/scroll-to.service";
import { HelperService } from "../services/helper.service";
import { overlayConfigFactory } from "ad-ngx-modialog";
import { Modal, BSModalContext } from "ad-ngx-modialog/plugins/bootstrap";
import { DownloadModalComponent } from "../download-modal/download-modal.component";

@Component({
  selector: "app-dashbard",
  templateUrl: "./dashbard.component.html",
  styleUrls: ["./dashbard.component.scss"],
  animations: [
    trigger("showChapter", [
      transition("void => *", [
        animate(
          ".5s ease",
          keyframes([
            style({
              height: "0px",
              opacity: "0",
              transform: "translateY(25px)",
              offset: 0
            }),
            style({
              height: "*",
              opacity: "1",
              transform: "translateX(0)",
              offset: 1
            })
          ])
        )
      ]),
      transition("* => void", [
        animate(
          ".3s ease",
          keyframes([
            style({
              height: "*",
              opacity: "1",
              transform: "translateY(0)",
              offset: 0
            }),
            style({
              height: "0px",
              opacity: "0",
              transform: "translateY(15px)",
              offset: 1
            })
          ])
        )
      ])
    ])
  ]
})
export class DashbardComponent implements OnInit {
  @Input() bookTitle: string;
  @Input() bookAuthor: string;
  @Input() bookId: string;
  @Input() chapters: any;
  dashboardOpen: boolean;
  activeChapterIndex: number;
  activeSubChapterId: number;
  activeLearningSectionId: number;
  initalTimeoutReady: boolean;
  modalOpen: boolean;
  searchMode: boolean;

  constructor(
    public _modal: Modal,
    private _scrollToService: ScrollToService,
    private helperService: HelperService
  ) {
    this.dashboardOpen = false;
    this.activeChapterIndex = 0;
    this.activeSubChapterId = 0;
    this.activeLearningSectionId = 0;
    this.initalTimeoutReady = false;
    this.modalOpen = false;
    this.searchMode = false;
  }

  ngOnInit() {
    const scrollToHash = this.getQueryHash();

    if (scrollToHash && scrollToHash.length) {
      const pos = this.getGlobalOffset(document.getElementById(scrollToHash))
        .top;
      if (pos) {
        this._scrollToService.scrollTo(pos, 0);
      }
    }

    this.helperService.filterData.subscribe(filterData => {
      if (filterData.isClosed !== undefined) {
        this.searchMode = !filterData.isClosed;
      }
    });

    this.helperService.filterData.subscribe(filterData => {
      if (filterData.isClosed !== undefined) {
        this.searchMode = !filterData.isClosed;
      }
    });

    this.helperService.activeSectionData.subscribe(activeSection => {
      // set current chapter
      if (activeSection.chapterIndex !== this.activeChapterIndex) {
        this.activeChapterIndex = activeSection.chapterIndex;
      }

      // set current subchapter
      if (activeSection.subChapterId === null && this.activeSubChapterId > 0) {
        this.activeSubChapterId = 0;
      } else if (
        activeSection.subChapterId &&
        this.activeSubChapterId !== activeSection.subChapterId
      ) {
        this.activeSubChapterId = activeSection.subChapterId;
      }

      // set current learning section id
      this.activeLearningSectionId = activeSection.learningSectionId;
    });

    this.helperService.dashbordStateData.subscribe(dashbordStateData => {
      if (
        !this.dashboardOpen &&
        dashbordStateData.open !== undefined &&
        dashbordStateData.open
      ) {
        this.dashboardOpen = true;
      }
    });

    this.helperService.setDashbordState(this.dashboardOpen);
  }

  dashBoardClickHandler() {
    if (!this.dashboardOpen) {
      this.initalTimeoutReady = false;
      this.dashboardOpen = true;
      this.helperService.setDashbordState(this.dashboardOpen);

      setTimeout(() => {
        this.initalTimeoutReady = true;
      }, 300);
    }
  }

  closeDashBoard() {
    this.dashboardOpen = false;
    setTimeout(() => {
      this.helperService.setDashbordState(this.dashboardOpen);
    }, 400);
  }

  clickedOutside(event: any) {
    if (this.dashboardOpen && this.initalTimeoutReady && !this.modalOpen) {
      this.closeDashBoard();
    }
  }

  scrollToLearningSection(id: string) {
    const pos = this.getGlobalOffset(document.getElementById(id)).top;
    if (pos) {
      this._scrollToService.scrollTo(pos, 0);
    }
    return false;
  }

  private getGlobalOffset(el) {
    let x = 0,
      y = 0;

    while (el) {
      x += el.offsetLeft;
      y += el.offsetTop;
      el = el.offsetParent;
    }
    return { left: x, top: y };
  }

  openDownloadModal() {
    this.modalOpen = true;
    return this._modal.open(
      DownloadModalComponent,
      overlayConfigFactory(
        { chapters: this.chapters, bookId: this.bookId, sourceComponent: this },
        BSModalContext
      )
    );
  }

  openDownloadChapterModal(subChapter: any, chapterId: number) {
    this.modalOpen = true;
    return this._modal.open(
      DownloadModalComponent,
      overlayConfigFactory(
        {
          subChapters: subChapter,
          bookId: this.bookId,
          chapterId: chapterId,
          sourceComponent: this
        },
        BSModalContext
      )
    );
  }

  private getQueryHash() {
    return window.location.hash.substr(1);
  }

  private modalClosedHandler() {
    this.modalOpen = false;
  }

  public enableSearch(event: any) {
    this.searchMode = true;
  }
}
