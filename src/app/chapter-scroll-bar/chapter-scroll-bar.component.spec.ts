import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChapterScrollBarComponent } from './chapter-scroll-bar.component';

describe('ChapterScrollBarComponent', () => {
  let component: ChapterScrollBarComponent;
  let fixture: ComponentFixture<ChapterScrollBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChapterScrollBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChapterScrollBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
