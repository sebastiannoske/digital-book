import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ChapterScrollBarService } from '../services/chapter-scroll-bar.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-chapter-scroll-bar',
  templateUrl: './chapter-scroll-bar.component.html',
  styleUrls: ['./chapter-scroll-bar.component.scss']
})
export class ChapterScrollBarComponent implements OnInit {
  chapterState: any;
  body: any;
  scrollBarHeight: number;
  top: number;
  maxTop: number;
  maxHeight: number;
  fixed: string;
  wrapTop: number;
  indicatorClass: string;

  constructor(private _chapterScrollBarService: ChapterScrollBarService, private _changeDetector: ChangeDetectorRef) {
    this.body = document.body;
    this.scrollBarHeight = 0;
    this.top = 0;
    this.maxHeight = 0;
    this.maxTop = 0;
    this.indicatorClass = environment.chapterIndicatorAsProgress ? 'progressbar' : 'scrollbar';

    window.removeEventListener('scroll', this.setScrollBarTop.bind(this), false);
    window.addEventListener('scroll', this.setScrollBarTop.bind(this), false);
  }

  ngOnInit() {
    this._chapterScrollBarService.currentState.subscribe(chapterState => {
      this.fixed = chapterState.fixed;

      if (this.fixed) {
        this.wrapTop = 15;
      } else if (this.chapterState) {
        const scrollTop = (window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop);

        if (scrollTop >= this.chapterState.offsetTop) {
          this.wrapTop = this.chapterState.offsetTop + this.chapterState.chapterHeight - document.documentElement.clientHeight + 15;
        } else {
          this.wrapTop = this.chapterState.offsetTop + 15;
        }
      }

      if (!this.chapterState || this.chapterState.chapterIndex !== chapterState.chapterIndex) {
        this.chapterState = chapterState;

        if (this.chapterState.chapterHeight) {
          if (environment.chapterIndicatorAsProgress) {
            this.initProgressBar();
          } else {
            this.initScrollBar();
          }
          this.setScrollBarTop();
        }
      }
    });
  }

  private initProgressBar() {
    this.scrollBarHeight = 0;
    this.maxHeight = document.documentElement.clientHeight - 30;
  }

  private initScrollBar() {
    this.scrollBarHeight = (document.documentElement.clientHeight / this.chapterState.chapterHeight)
      * (document.documentElement.clientHeight - 30);
    this.maxTop = document.documentElement.clientHeight - 30 - this.scrollBarHeight;
  }

  setScrollBarTop() {
    if (!environment.chapterIndicatorAsProgress && this.chapterState && this.chapterState.chapterHeight && this.chapterState.offsetTop) {
      const scrollTop = (window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop);

      if (scrollTop >= this.chapterState.offsetTop
        && scrollTop <= this.chapterState.offsetTop + this.chapterState.chapterHeight - document.documentElement.clientHeight) {
        const scrollInChapter = scrollTop - this.chapterState.offsetTop;
        const percentage = scrollInChapter / (this.chapterState.chapterHeight - document.documentElement.clientHeight);
        this.top = this.maxTop * percentage;
      }

      this._changeDetector.markForCheck();
    }

    if (environment.chapterIndicatorAsProgress && this.chapterState && this.chapterState.chapterHeight && this.chapterState.offsetTop) {
      const scrollTop = (window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop);

      if (scrollTop >= this.chapterState.offsetTop
        && scrollTop <= this.chapterState.offsetTop + this.chapterState.chapterHeight - document.documentElement.clientHeight) {
        const scrollInChapter = scrollTop - this.chapterState.offsetTop;
        const percentage = scrollInChapter / (this.chapterState.chapterHeight - document.documentElement.clientHeight);
        this.scrollBarHeight = this.maxHeight * percentage;
      } else if (scrollTop < this.chapterState.offsetTop) {
        this.scrollBarHeight = 0;
      }

      this._changeDetector.markForCheck();
    }
  }
}
