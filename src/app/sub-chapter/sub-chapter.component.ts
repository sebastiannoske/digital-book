import { Component, OnInit, Input } from '@angular/core';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-sub-chapter',
  templateUrl: './sub-chapter.component.html',
  styleUrls: ['./sub-chapter.component.scss']
})
export class SubChapterComponent implements OnInit {
  @Input() subChapter: any;
  @Input() chapterIndex: any;
  @Input() chapterId: number;

  constructor(private _helperService: HelperService) { }

  ngOnInit() {}

  setActiveSection(inViewport: boolean) {
    if (inViewport) {
        this._helperService.setActiveSection({
          chapterIndex: this.chapterIndex - 1,
          subChapterId: this.subChapter.id,
          learningSectionId: null
        });
    }
  }
}
