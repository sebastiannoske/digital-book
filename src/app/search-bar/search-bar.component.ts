import { Component, OnInit, Input, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { HelperService } from '../services/helper.service';
import { ScrollToService } from '../services/scroll-to.service';

import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;
  @Input() setFocus: boolean;
  filter: string;
  searchDomResults: any;
  currentSearchItemIndex: number;
  lastFoundElement: any;

  constructor(
    private _scrollToService: ScrollToService,
    private helperService: HelperService,
    private _changeDetector: ChangeDetectorRef
  ) {
    this.filter = '';
    this.searchDomResults = null;
    this.currentSearchItemIndex = 0;
    this.lastFoundElement = null;
  }

  ngOnInit() {
    this.helperService.filterData.subscribe(filterData => {
      if (filterData.value !== undefined) {
        this.filter = filterData.value;
      }
    });

    this.helperService.domResultsData.subscribe(domResultsData => {
      if (domResultsData.domResults !== undefined) {
        this.searchDomResults = domResultsData.domResults;
        this.currentSearchItemIndex = 0;
      }
      if (!this.lastFoundElement && this.searchDomResults) {
        this.lastFoundElement = this.searchDomResults[this.currentSearchItemIndex];
      }
    });

    this.helperService.indexData.subscribe(indexData => {
      if (indexData.value !== undefined && indexData.value !== this.currentSearchItemIndex) {
        this.currentSearchItemIndex = indexData.value;
      }
    });

    const obs = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      debounceTime(250)
    );

    obs.subscribe((result) => this.setFilter());

    if (this.setFocus) {
      this.searchInput.nativeElement.focus();
    }
  }

  private setFilter() {
    this.helperService.setSearchObject({
      filter: this.filter,
      isClosed: false
    });
    this._changeDetector.markForCheck();
    this.currentSearchItemIndex = 0;

    setTimeout(() => {
      this.searchDomResults = document.querySelectorAll(
        'h2 > span.highlight, p > span.highlight, p > a.keyword > span:first-child > span.highlight, .quote-wrap > span > span.highlight');
      this.helperService.setSearchDomResults(this.searchDomResults);
      if (this.searchDomResults && this.searchDomResults.length) {
        this._changeDetector.markForCheck();
        this.scrollToSearchItem(0);
      }
    }, 500);
  }

  public scrollToPrevItem() {
    this.currentSearchItemIndex = this.currentSearchItemIndex > 0
      ? this.currentSearchItemIndex - 1
      : this.searchDomResults.length - 1;
    this.helperService.setSearchDomResultsIndex(this.currentSearchItemIndex);
    this.scrollToSearchItem(0);
  }

  public scrollToNextItem() {
    this.currentSearchItemIndex = this.currentSearchItemIndex < this.searchDomResults.length - 1
      ? this.currentSearchItemIndex + 1
      : 0;
    this.helperService.setSearchDomResultsIndex(this.currentSearchItemIndex);
    this.scrollToSearchItem(0);
  }

  private scrollToSearchItem(animationDuration: number) {
    const pos = this.getGlobalOffset(this.searchDomResults[this.currentSearchItemIndex]).top;

    if (this.lastFoundElement) {
      this.lastFoundElement.classList.remove('focused');
    }
    this.lastFoundElement = this.searchDomResults[this.currentSearchItemIndex];

    if (this.lastFoundElement) {
      this.lastFoundElement.classList.add('focused');
    }

    if (pos) {
      // this.dashboardOpen = false;
      this._scrollToService.scrollTo(pos - 100, animationDuration);
    }
  }

  public clearFilter() {
    this.filter = '';
    this.helperService.setSearchObject({
      filter: this.filter,
      isClosed: true
    });
    if (this.lastFoundElement) {
      this.lastFoundElement.classList.remove('focused');
    }
  }

  private getGlobalOffset(el) {
    let x = 0, y = 0;

    while (el) {
      x += el.offsetLeft;
      y += el.offsetTop;
      el = el.offsetParent;
    }
    return { left: x, top: y };
  }
}
