import { throwError as observableThrowError, Observable, BehaviorSubject } from 'rxjs';

import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class DataService {
	private _eventsUrl = 'https://ifso.sowi.uni-due.de/oer/api'; // 'http://oer-back-end.test/api';
	private _headers: HttpHeaders;
	private _fileHeaders: HttpHeaders;
	private _options: any;
	private _bookData: BehaviorSubject<any> = new BehaviorSubject({});

	constructor(private _http: HttpClient) {
		this._headers = this.getHttpHeaders();
		this._fileHeaders = this.getHttpFileHeaders();
		this._options = {
			headers: this._fileHeaders
		};
	}

	public getData(id: number) {
		this.fetchData(id).subscribe(data => {
			this._bookData.next(data);
		});
	}

	public get bookData(): Observable<any> {
		return this._bookData;
	}

	private getHttpHeaders(): HttpHeaders {
		return new HttpHeaders()
			.set('Authorization', 'Bearer xFhQsoqlboBFAX7ENajMJaMkKjS53X8MirvuIbUOg35J8DKT3bQe3Kr5wue5')
			.append('Content-Type', 'application/json')
			.append('Accept', 'application/json');
	}

	private getHttpFileHeaders(): HttpHeaders {
		return new HttpHeaders()
			.set('Authorization', 'Bearer xFhQsoqlboBFAX7ENajMJaMkKjS53X8MirvuIbUOg35J8DKT3bQe3Kr5wue5')
			.append('Content-Type', 'application/odt')
			.append('Accept', 'application/odt');
	}

	private fetchData(id: number): Observable<any> {
		const url = `${this._eventsUrl}/books/${id}`;
		// const url = 'assets/api/book.json';

		return this._http.get(url).pipe(catchError(this.handleError));
	}

	public fetchPageContentData(pageConent: string): Observable<any> {
		const url = `${this._eventsUrl}/books/1/${pageConent}`;
		// const url = 'assets/api/book.json';

		return this._http.get(url).pipe(catchError(this.handleError));
	}

	public generateDownload(id: number): Observable<any> {
		const url = `${this._eventsUrl}/books/${id}/generate-docx`;

		return this._http
			.post(url, { responseType: 'blob' }, this._options)
			.pipe(catchError(this.handleError));
	}

	private handleError(err: HttpErrorResponse) {
		let errorMessage = '';
		if (err.error instanceof Error) {
			errorMessage = `An error occurred: ${err.error.message}`;
		} else {
			errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
		}
		console.error(errorMessage);
		return observableThrowError(errorMessage);
	}
}
